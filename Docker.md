Docker
=====

A simple Docker image can be built

```sh
docker build -t check-soa .
```

Without arguments, running it provides the help

```sh
docker run --rm check-soa
Usage of /usr/src/app/check-soa:
/usr/src/app/check-soa [options] ZONE
  -4	Use only IPv4
  -6	Use only IPv6
  -a	Do not require an authoritative answer
…
```

You can run real test with `/usr/src/app/check-soa [options] ZONE`

```sh
docker run --rm check-soa /usr/src/app/check-soa -d -tcp -nsid bortzmeyer.org.
2024/12/16 09:54:15 check-soa: Last commit on HEAD 0bcc50900d826a1baba93d0f3419c54665b5709a on 2024-12-16 10:38:14 +0100
2024/12/16 09:54:15 using zone nameservers
2024/12/16 09:54:15 DEBUG end of DNS request "bortzmeyer.org." / 2
2024/12/16 09:54:15 DEBUG end of DNS request "ns4.bortzmeyer.org." / 1
2024/12/16 09:54:15 DEBUG Querying SOA from 92.243.4.211:53
2024/12/16 09:54:15 DEBUG end of DNS request "ns2.bortzmeyer.org." / 28
2024/12/16 09:54:15 DEBUG Querying SOA from [2400:8902::f03c:91ff:fe69:60d3]:53
2024/12/16 09:54:15 DEBUG end of DNS request "ns1.bortzmeyer.org." / 1
2024/12/16 09:54:15 DEBUG end of DNS request "ns1.bortzmeyer.org." / 28
…
```
